package org.komissarova.many_files;

import java.util.ArrayList;

/**
 * ШАГ 1. Получение чисел с помощью рандома и их запись в файл
 * 
 * @author Комиссарова М.Е., 16ИТ18к
 */
public class Stage1 {
    /**
     * Кол-во чисел
     */
    public static final int COUNT = 1_000; 
    
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>(COUNT);
        
        generateNumbers(numbers);
        System.out.println("Получены числа: " + numbers);
        NumberIO.write(numbers, "intdata.dat");
    }
    
    /**
     * Заполняет список numbers случайными числами от 10 до 10 млн
     * 
     * @param numbers список для хранения чисел
     */
    public static void generateNumbers(ArrayList<Integer> numbers) {
         for(int i = 0; i < COUNT; i++) {
            numbers.add(10 + (int)(Math.random() * 10_000_000));
        }
    }
}
