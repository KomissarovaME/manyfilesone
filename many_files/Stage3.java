package org.komissarova.many_files;

import java.util.ArrayList;

/**
 * ШАГ 3. Получение чисел с помощью чтения, удаление не счастливых и запись оставшихся в файл
 * 
 * @author Комиссарова М.Е., 16ИТ18к
 */
public class Stage3 {
    public static void main(String[] args) {
        ArrayList<Integer> numbers = NumberIO.read("int6data.dat");
        System.out.println("Прочитаны шестизначные числа: " + numbers);
        numbers = leaveOnlyLucky(numbers);
        System.out.println("Поулчены шестизначные счастливые числа: " + numbers);
        NumberIO.writeAsText(numbers, "lucky.txt");
    }

    /**
     * Возвращает true, если число счастливое, т.е. сумма первых трёх его цифр равна сумме последних трёх
     * false в любом другом случае
     * 
     * Значения разрядов находятся следующим образом:
     * единицы - остаток от деления числа на 10
     * десятки - частное от деления остатка от деления числа на 100 на 10
     * сотни - частное от деления остатка от деления числа на 1000 на 100
     * тысячи - частное от деления остатка от деления числа на 10 000 на 1 000
     * десятки тысяч - частное от деления остатка от деления числа на 100 000 на 10 000
     * сотни тысяч - частное от деления числа на 100 000
     * 
     * @param number числоа для проверки на счастливость
     * @return true, если число счастливое, т.е. сумма первых трёх его цифр равна сумме последних трёх
     */
    public static boolean isLucky(int number) {
        int a = number / 100_000;
        int b = number % 100_000 / 10_000;
        int c = number % 10_000 / 1_000;
        int d = number % 1_000 / 100;
        int e = number % 100 / 10;
        int f = number % 10;
        
        return (a + b + c) == (d + e + f);
    }

    /**
     * Создаёт новый спсиок чисел, содержащий только счастливые числа
     * 
     * @param numbers список шестизначных чисел
     * @return список счастливых чисел из списка numbers
     */
    public static ArrayList<Integer> leaveOnlyLucky(ArrayList<Integer> numbers) {
        ArrayList<Integer> newNumbers = new ArrayList<Integer>();
        
        for(int number : numbers) {
            if(isLucky(number)) {
                newNumbers.add(number);
            }
        }
        
        return newNumbers;
    }
}
