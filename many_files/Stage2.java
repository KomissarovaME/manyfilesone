package org.komissarova.many_files;

import java.util.ArrayList;

/**
 * ШАГ 2. Получение чисел с помощью чтения, удаление не шестизначных и запись оставшихся в файл
 * 
 * @author Комиссарова М.Е., 16ИТ18к
 */
public class Stage2 {

    public static void main(String[] args) {
        ArrayList<Integer> numbers = NumberIO.read("intdata.dat");
        System.out.println("Прочитаны числа: " + numbers);
        numbers = leaveOnlyInt6(numbers);
        System.out.println("Получены шестизначные числа: " + numbers);
        NumberIO.write(numbers, "int6data.dat");
    }

    /**
     * Создаёт новый спсиок чисел, содержащий только шестизначные числа
     * 
     * @param numbers список рандомных чисел
     * @return список шестизначных чисел из списка numbers
     */
    public static ArrayList<Integer> leaveOnlyInt6(ArrayList<Integer> numbers) {
        ArrayList<Integer> newNumbers = new ArrayList<Integer>();
        
        for(int number : numbers) {
            if(number >= 100_000 && number < 1_000_000) {
                newNumbers.add(number);
            }
        }
        
        return newNumbers;
    }
}
