package org.komissarova.many_files;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class NumberIO {

    /**
     * Записывает числа из спиcка numbers в файл fileName 
     *
     * @param fileName имя файла, в который будут записаны данные числа
     */
    public static ArrayList<Integer> read(String fileName) {
        ArrayList<Integer> numbers = new ArrayList<>();
        
        try(DataInputStream in = new DataInputStream(new FileInputStream(fileName))) {
            while(in.available() > 0) {
                numbers.add(in.readInt());
            }
        } catch (IOException exception) {
            System.out.println("Что-то пошло не так");
        }
        
        return numbers;
    }
    /**
     * Записывает числа из спиcка numbers в файл fileName в виде строк
     * FileWriter позволяет записывать данные в текстовый файл. С его помощью невозможно записать число как последовательность байт,
     * а только как строку. Отлично подходит для представления результатов работы нашей программы
     * 
     * @param numbers список чисел
     * @param fileName имя файла, в который будут записаны данные числа
     */
    public static void writeAsText(ArrayList<Integer> numbers, String fileName) {
        try(FileWriter writer = new FileWriter(fileName)) {
            for(Integer number : numbers) {
                writer.write(number + "\n");
            }
        } catch (IOException exception) {
            System.out.println("Что-то пошло не так");
        }
    }

    /**
     * Записывает числа из спиcка numbers в файл fileName в вице чисел
     * DataOutputStream - поток, создаваемый на основе файлового потока FileOutputStream
     * DataOutputStream позволяет легко записывать примитивные типы вроде int, double, byte и т.д. в файлы
     * 
     * @param numbers список чисел
     * @param fileName имя файла, в который будут записаны данные числа
     */
    public static void write(ArrayList<Integer> numbers, String fileName) {
        try(DataOutputStream out = new DataOutputStream(new FileOutputStream(fileName))) {
            for(Integer number : numbers) {
                out.writeInt(number);
            }
        } catch (IOException exception) {
            System.out.println("Что-то пошло не так");
        }
    }

}
